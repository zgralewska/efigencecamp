window.addEventListener('load', function(){
    places.init(null);
});
var places = {
    currentPage: 0,
    container: null,

    paginationNumber:
    {
        setEventNumber: function(){
            var th = this,
                clickedNumber;

            clickedNumber = th.id;
            window.places.init(clickedNumber);
        },
        init: function(){
            var th=this,
                container,
                pages,
                i,
                pageThis;
            container = document.getElementById("pagination");
            pages = container.getElementsByTagName("a");

            for(i=0; i < pages.length; i++){
                pageThis = pages[i];
                pageThis.onclick = th.setEventNumber;
            }
        }
    },
    setActivePaginationNumber: function (paginationNumbers) {
        var th = this,
            activeClass = 'active-page',
            disabledButton = 'disabled-button',
            previous,
            next,
            actualPage;

        actualPage = document.getElementById(String(th.currentPage));
        previous = document.getElementById('prevPage');
        next = document.getElementById('nextPage');
        actualPage.className = activeClass;

        if (th.currentPage == 1) {
            previous.disabled = true;
            previous.className = disabledButton;
        }
        else if (th.currentPage == paginationNumbers) {
            next.disabled = true;
            next.className = disabledButton;
        }
        else {
            previous.disabled = false;
            previous.className = "";
            next.disabled = false;
            next.className = "";

        }
        th.paginationNumber.init();
    },
    displayPagination: function (paginationNumbers) {
        var th = this,
            navPagination,
            x,
            result;

        navPagination = document.getElementById('paginationNumbers');
        result = "";

        for (x = 1; x <= paginationNumbers; x++) {
            result += '<a id="' + x + '">' + x + '</a>';
        }
        navPagination.innerHTML = result;
        th.setActivePaginationNumber(paginationNumbers);
    },
    getIcons: function (icons) {
        var result = "",
            iconsObjects,
            i;

        iconsObjects = {
            airport: '<div class="tooltip-wrapper"><span class="icon-airplane"></span><div class="tooltip">Lotnisko</div></div>',
            food: '<div class="tooltip-wrapper"><span class="icon-spoon-knife"></span><div class="tooltip">Jedzenie</div></div>',
            parking: '<div class="tooltip-wrapper"><span class="icon-truck"></span><div class="tooltip">Parking</div></div>',
            paypas: '<div class="tooltip-wrapper"><span class="icon-credit-card"></span><div class="tooltip">Paypas</div></div>',
            swimming: '<div class="tooltip-wrapper"><span class="icon-lifebuoy"></span><div class="tooltip">Basen</div></div>',
            tv: '<div class="tooltip-wrapper"><span class="icon-tv"></span><div class="tooltip">TV</div></div>',
            wifi: '<div class="tooltip-wrapper"><span class="icon-connection"></span><div class="tooltip">WiFi</div></div>'
        };

        for (i in icons) {
            if (icons.hasOwnProperty(i) && icons[i]) {
                result += iconsObjects[i];
            }
        }
        return ['<div>', result, '</div>'].join('');
    },

    getStars: function (counter) {
        var star = '<span class="icon-star-full"></span>';
        var result = "<div>";

        for (var i = 0; i < counter; i++) {
            result += star;
        }
        return result + "</div>";
    },

    parseResults: function (opts) {
        var th = this,
            o = {
                results: (opts && opts.results) || null
            },
            places,
            placesAmount,
            i,
            icons,
            paginationNumbers,
            html = [];

        if (o.results) {
            places = o.results.places;
            placesAmount = places.length;
            paginationNumbers = o.results.total_pages;

            for (i = 0; i < placesAmount; i++) {
                icons = {
                    airport: places[i].airport,
                    food: places[i].food,
                    parking: places[i].parking,
                    paypas: places[i].paypas,
                    swimming: places[i].swimming,
                    tv: places[i].tv,
                    wifi: places[i].wifi
                };

                html = [html,
                    '<section class="row place">',
                    '<section class="col-md-1"></section>',
                    '<section id="photo" class="col-xs-12 col-sm-4 col-md-3 photo-mini">',
                    '<img src=' + places[i].image + ' alt="krajobraz">',
                    '</section>',
                    '<section id="content" class="col-xs-12 col-sm-3 col-md-4">',
                    '<h3 class="content-title">',
                    places[i].name,
                    '</h3>',
                    '<h4 class="content-subtitle">',
                    places[i].place + ", "
                    + '<span>' + places[i].district + '</span>',
                    '</h4>',
                    '<div class="content-start">',
                    th.getStars(places[i].starts),
                    '</div>',
                    '<article>',
                    '<p class="content-text">',
                    places[i].description,
                    '</p>',
                    '<a href="#" class="visible-anchor">Szczegóły...</a>',
                    '</article>',
                    '</section>',
                    '<section class="col-xs-12 col-sm-2 col-md-1 icons">',
                    '<figure>',
                    th.getIcons(icons),
                    '</figure>',
                    '</section>',
                    '<section class="col-xs-12 col-sm-3 col-md-2 costs">',
                    '<div>',
                    '<h4 class="score">',
                    'Ocena: ' + places[i].score + "/10",
                    '</h4>',
                    '<h6 class="score-desc">',
                    'Ocena na podstawie ' + places[i].opinion_count + " opinii",
                    '</h6>',
                    '</div>',
                    '<div>',
                    '<h4 class="cost-before">',
                    places[i].oldprice.toFixed(2),
                    '<span>PLN</span>',
                    '</h4>',
                    '<h3 class="cost-after">',
                    places[i].price.toFixed(2),
                    '<span>PLN</span>',
                    '</h3>',
                    '<h6 class="cost-desc">',
                    'Cena za <strong>3 noce</strong>',
                    '</h6>',
                    '</div>',
                    '<div class="button-wrapper">',
                    '<button>Zarezerwuj teraz</button>',
                    '</div>',
                    '</section>',
                    '<section class="col-md-1"></section>',
                    '</section>'
                ].join('');
            }
            th.container.innerHTML = html;
            th.displayPagination(paginationNumbers);
        }
    },

    ajaxCallback: function (opts) {
        var th = this,
            o,
            jsonResult;

        o = {
            statusCode: (opts && opts.statusCode) || null,
            responseTxt: (opts && opts.responseText) || null
        };

        if (o.statusCode == 200) {
            jsonResult = eval('(' + o.responseTxt + ')');
            th.parseResults({
                results: jsonResult
            });

        }
    },

    getPlaces: function (opts) {
        var th = this,
            o;

        o = {
            url: (opts && opts.url) || null,
            currPage: (opts && opts.currPage) || 1
        };

        if (o.url) {
            th.currentPage = parseInt(o.currPage, 10);
            makeRequestGET(o.url, {page: o.currPage}, th);
        }
    },

    init: function (currPage) {
        var domain = "http://camp.efigence.com",
            th = this;

        th.container = document.getElementById("places");

        this.getPlaces({
            url: domain + "/camp/api/places",
            currPage: currPage
        })
    }
};