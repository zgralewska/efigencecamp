/**
 * Created by DELL on 2015-09-18.
 */
function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}
function findSubstrRemove(name, part){
    var index = name.indexOf(part);
    var result;
    if (index < 0) {
        result = name;
    } else {
        result = name.substr(0, index);
    }
    return result;
}
function manageHover(width) {
    var bodyClass,
        bodyElement;
    bodyElement = document.getElementsByTagName('body')[0];
    bodyClass = bodyElement.className;

    if (width < 993) {
        bodyElement.className = findSubstrRemove(bodyClass, " desktop");
    }
    else {
        if(bodyClass.indexOf("desktop") < 0){
            bodyElement.className += " desktop";
        }
    }
}