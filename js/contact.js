var contact = {
    url : "http://camp.efigence.com/camp/api/contact",

    displayValidationErrors: function(responseText){
        var results,
            valInfo;

        results = eval('(' + responseText + ')').errors;

        for (var k in results) {
            if (results.hasOwnProperty(k)) {
                document.getElementById(k+"Valid").innerHTML = "Pole " +results[k];
                document.getElementById(k+"Valid").style.display = 'block';
            }
        }
    },

    ajaxCallback: function(opts){
        var response,
            responseWrapper,
            statusCode,
            responseTxt,
            form;

        statusCode = opts.statusCode;
        responseTxt = opts.responseText;
        responseWrapper = document.getElementById("infoContact");

        switch(statusCode){
            case 200:
                response = "Wiadomość pomyślnie wysłana";
                form = document.getElementById("formContent");
                form.reset();
                break;
            case 422:
                this.displayValidationErrors(responseTxt);
                response = "Formularz nie został wysłany. Popraw poniższe błędy";
                break;
            case 500:
                response = "Błąd serwera. Spróbuj jeszcze raz";
                break;
            default:
                response = "Niespodziewany błąd nr:" + statusCode + ". Spróbuj później";
                break;
        }
        responseWrapper.innerHTML = response;
        responseWrapper.style.display="block";

        setTimeout(function(){
            responseWrapper.style.display="none";
        }, 5000);
    },

    sendContact: function(opts){
        if(opts){
            makeRequestPOST(this.url, opts, this);
        }
    },


    init: function(opts){
        var valInfo = document.getElementsByClassName('validation-info');
        for(var x=0; x < valInfo.length; x++){
            valInfo[x].innerHTML = "";
            valInfo[x].style.display = 'none';
        }

        this.sendContact({
            name : (opts && opts.name) || "",
            email: (opts && opts.email) || "",
            message: (opts && opts.message) || ""
        })
    }
};
