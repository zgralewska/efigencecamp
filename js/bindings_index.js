/**
 * Created by DELL on 2015-09-09.
 */
window.addEventListener('resize', function(){
    manageHover(window.innerWidth);
    checkWidth();
});
window.addEventListener('load', function() {
    manageHover(window.innerWidth);
    videoHandler.init();
    sliderHandler.init();
    contactSubmit.init();


    document.onkeydown = function(evt) {
        var video,
            children,
            container,
            photo,
            i,
            bodyElement,
            videoWrapper,
            videoElelement;

        videoWrapper = document.getElementById('video-film');
        videoElelement = document.getElementById('v');
        container = document.getElementById('video');

        photo = document.getElementById('overlay');
        bodyElement = document.getElementsByTagName('body')[0];

        evt = evt || window.event;
        if (evt.keyCode == 27) {
            if(videoElelement) {
                videoElelement.pause();
                videoWrapper.style.opacity=0;
                videoWrapper.addEventListener('transitionend', function()
                {
                    container.removeChild(videoWrapper);

                    children = container.childNodes;
                    for (i = 0; i < children.length; i++) {
                        if (children[i].style) {
                            children[i].style.display = 'block';
                        }
                    }
                    videoHandler.init();
                }, false);
            }
            if(photo){
                bodyElement.removeChild(photo);
            }
        }
    };
});

var contactSubmit = {
    setEvent: function(e){
        e.preventDefault();

        var name,
            email,
            msg;

        name = document.getElementById("name").value;
        email = document.getElementById("email").value;
        msg = document.getElementById("message").value;

        window.contact.init({
            name: name,
            email: email,
            message: msg
        })
    },
    init: function(){
        var th=this,
            contactBttn = document.getElementById("contactSubmit");

        contactBttn.onclick = th.setEvent;
    }
};
var videoHandler = {
    container: null,

    setEvent: function(){
        var
            th=videoHandler,
            children,
            video,
            videoWrapper,
            videoElement,
            readyPlay = false,
            readyTrans,
            i;

        video = '<section class="col-xs-12" id="video-film">\
                    <video id="v" muted controls>\
                        <source src="http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4" type="video/mp4">\
                    </video>\
            </section>';

        children = th.container.childNodes;

        for(i=0; i < children.length; i++) {
            if (children[i].style) {
                children[i].style.display = 'none';
            }
        }
        th.container.innerHTML += video;
        videoWrapper = document.getElementById('video-film');
        videoElement = videoWrapper.getElementsByTagName('video')[0];

        //Niewazne ktory listener pierwszy, video zawsze sie odpali
        videoWrapper.addEventListener('transitionend', function(){
            readyTrans = true;
            if(readyPlay){
                videoElement.play();
            }
        }, false);
        videoElement.addEventListener('loadeddata', function() {
            readyPlay = true;
            if(readyTrans){
                videoElement.play();
            }
        }, false);

        getComputedStyle(videoElement).getPropertyValue("opacity");
        videoWrapper.style.opacity=1;
    },

    init: function(){
        var
            th = this,
            container,
            vBttn;

        container = document.getElementById('video');
        vBttn = container.getElementsByTagName('button')[0];
        th.container = container;

        vBttn.onclick = th.setEvent;
    }
};

var sliderHandler = {
    container: null,
    dot: 0,
    setActiveDot: function(type, maxLength){
        var container = document.getElementById('sliderDots'),
            dots = container.getElementsByTagName('span'),
            i,
            activeDot,
            context = sliderHandler;

        if(type == "right"){
            if(context.dot == maxLength-1){
                context.dot = 0;
            }
            else{
                context.dot++;
            }
        }
        else if(type == "left"){
            if(context.dot == 0){
                context.dot = maxLength-1;
            }
            else{
                context.dot--;
            }
        }
        activeDot = dots[context.dot];

        if(activeDot){
            for(i=0; i< dots.length;i++){
                dots[i].className = 'slider-dot';
            }
            activeDot.className = 'slider-dot-active';
        }
    },
    slidesAnimation : {
        transitionFlag: 0,
        opts: null,

        fadeOut: function(opts){
            var context = this,
            o = {
                blocks: (opts && opts.blocks) || null,
                slides: (opts && opts.slides) || null,
                changeOrder: (opts && opts.changeOrder) || null,
                loopStart: (opts && opts.loopStart) || 0,
                loopEnd: (opts && opts.loopEnd) || 0,
                slideWidth: (opts && opts.slideWidth) || null
            };

            //Dla fadeOut -> 0
            context.transitionFlag = 0;

            if(o.slides){
                context.opts = o;

                o.slides.newNode.addEventListener("transitionend", function(){
                    if(context.transitionFlag == 0){
                        context.move();
                    }
                    else if(context.transitionFlag == 1){
                        o.slides.newNode.style.transition = "none";
                    }
                }, false);
                getComputedStyle(o.slides.newNode).getPropertyValue("opacity");
                o.slides.newNode.style.opacity = 0;
                o.slides.newNode.style.transition = "opacity 600ms";
            }
        },

        move: function(){
            var context = this;

            context.opts.blocks[context.opts.loopEnd].addEventListener("transitionend", context.fadeIn, false);
            for(var i=context.opts.loopStart; i>=context.opts.loopEnd;i--){
                context.opts.blocks[i].style.transform = "translate("+context.opts.slideWidth+"px,0)";
                context.opts.blocks[i].style.transition = "all 1s ease-in-out";
            }
        },

        fadeIn: function() {
            var context = sliderHandler.slidesAnimation;

            context.opts.blocks[context.opts.loopEnd].removeEventListener("transitionend", context.fadeIn, false);
            for (var i = context.opts.loopStart; i >= context.opts.loopEnd; i--) {
                context.opts.blocks[i].style.transform = "none";
                context.opts.blocks[i].style.transition = "none";
                getComputedStyle(context.opts.blocks[i]).getPropertyValue("transform");
            }
            context.opts.changeOrder(callback);

            function callback() {
                //Dla fadeIn -> 1
                context.transitionFlag = 1;
                getComputedStyle(context.opts.slides.newNode).getPropertyValue("opacity");
                context.opts.slides.newNode.style.opacity = 1;
                context.opts.slides.newNode.style.transition = "opacity 600ms";
            }
        }
    },

    moveSlides: function(slides, type){
        var
            th = this,
            blocks,
            context = sliderHandler,
            slideWidth,
            changeOrder,
            loopStart,
            loopEnd,
            transitionFlag,
            slidesContainer = document.getElementById('sliderAttractions'),
            slidesAnimation;

        blocks = context.container.getElementsByTagName('section');

        //previous
        if(type == "left"){
            slideWidth = blocks[0].offsetWidth;
            loopStart = blocks.length-2;
            loopEnd = 0;
            changeOrder = function(callback){
                slidesContainer.insertBefore(slides.newNode, slides.nodeAfter);
                callback();
            };
        }
        //next
        else if("right"){
            slideWidth = -1*blocks[0].offsetWidth;
            loopStart = blocks.length-1;
            loopEnd = 1;
            changeOrder = function(callback){
                insertAfter(slides.newNode, slides.nodeAfter);
                callback();
            };
        }
        context.slidesAnimation.fadeOut({
            blocks: blocks,
            slides: slides,
            changeOrder: changeOrder,
            loopStart: loopStart,
            loopEnd: loopEnd,
            slideWidth: slideWidth
        });
    },
    setEvent: function(){
        var th=this,
            context = sliderHandler,
            arrowId,
            sectionToMove,
            blocks,
            type;

        sectionToMove = {newNode: "", nodeAfter: ""};
        blocks = context.container.getElementsByTagName('section');
        arrowId  = th.id;

        if(arrowId == 'nextSlide'){
            sectionToMove.newNode = blocks[0];
            sectionToMove.nodeAfter = blocks[blocks.length-1];

            type = "right";
        }
        else if(arrowId == 'previousSlide'){
            sectionToMove.newNode = blocks[blocks.length-1];
            sectionToMove.nodeAfter = blocks[0];

            type = "left";
        }
        context.moveSlides(sectionToMove, type);
        context.setActiveDot(type, blocks.length);
    },
    init: function(){
        var th=this,
            container,
            arrows,
            blocks,
            i;

        container = document.getElementById('slider');
        arrows = container.getElementsByTagName('button');

        th.container = container;

        for(i=0; i < arrows.length; i++) {
            arrows[i].onclick = th.setEvent;
        }
    }
};

