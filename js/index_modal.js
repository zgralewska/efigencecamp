/**
 * Created by DELL on 2015-09-15.
 */
window.addEventListener('load', function() {
    modalAttractions.init();
});
var modalAttractions = {
    figureUrls:null,

    setModal: function(){
        var modal,
            modalElement,
            bodyElement,
            context = modalAttractions,
            url;

        modal = '<div>\
                     <img src="">\
                 </div>';

        bodyElement = document.getElementsByTagName('body')[0];
        modalElement = document.getElementById('overlay');

        if(!modalElement){
            modalElement = document.createElement('div');
            modalElement.id = "overlay";
        }
        modalElement.innerHTML = modal;

        for (var key in context.figureUrls) {
            if (Object.prototype.hasOwnProperty.call(context.figureUrls, key)) {
                if(key == this.className){
                    url = context.figureUrls[key];
                }
            }
        }
        modalElement.getElementsByTagName('img')[0].src = '../' + url.substring(url.indexOf('static'), url.length-1);
        bodyElement.appendChild(modalElement);

        modalElement.style.visibility = (modalElement.style.visibility == "visible") ? "hidden" : "visible";
    },
    init: function(){
        var th=this,
            attractions,
            figure,
            photos,
            singlePhoto,
            i,
            figureUrls = [];

        attractions = document.getElementsByClassName('attraction-wrapper');
        photos = document.getElementsByClassName('photo-wrapper');

        for(i=0; i < attractions.length; i++){
            figure = attractions[i].getElementsByTagName('figure')[0];
            figure.onclick = th.setModal;

            figureUrls[figure.className] = window.getComputedStyle(figure).getPropertyValue("background").match(/url\(([^)]+)\)/i)[0];
        }
        for(i=0; i < photos.length; i++){
            singlePhoto = photos[i].getElementsByTagName('div')[0];
            singlePhoto.onclick = th.setModal;

            figureUrls[singlePhoto.className] = window.getComputedStyle(singlePhoto).getPropertyValue("background").match(/url\(([^)]+)\)/i)[0];
        }

        th.figureUrls = figureUrls;
    }
};