/**
 * Created by DELL on 2015-09-01.
 */
//AIzaSyDflBE7du40oO9-n_xQ4kSXYcuVqjQoed8
function initMap() {
    var location = {lat: 52.184931, lng: 21.001026};

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: new google.maps.LatLng(52.179248, 21.024753),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel:false
    });

    var marker = new google.maps.Marker({
        position: location,
        map: map,
        title: 'Artegence Sp. z o.o.'
    });
}
