/**
 * Created by DELL on 2015-09-02.
 */

function makeRequestGET(_url, params, o)  {
        var http_request = false;
        var ajaxParams = "";
        var url = _url;

        if (window.XMLHttpRequest) { // Mozilla, Safari,...
            http_request = new XMLHttpRequest();
            if (http_request.overrideMimeType) {
                http_request.overrideMimeType('text/xml');
                // Przeczytaj o tym wierszu poniżej
            }
        } else if (window.ActiveXObject) { // IE
            try {
                http_request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    http_request = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {
                }
            }
        }

        if (!http_request) {
            alert('Poddaję się :( Nie mogę stworzyć instancji obiektu XMLHTTP');
            return false;
        }
        http_request.onreadystatechange = function () {
            alertContents(http_request, o);
        };

        ajaxParams = prepareParams(params);

        if(ajaxParams){
            url += "?"+ajaxParams;
        }
        http_request.open('GET', url, true);
        http_request.send();

    }

function  makeRequestPOST(url, params, o) {

        var http_request = false;
        var ajaxParams = "";

        if (window.XMLHttpRequest) { // Mozilla, Safari,...
            http_request = new XMLHttpRequest();
            if (http_request.overrideMimeType) {
                http_request.overrideMimeType('text/xml');
                // Przeczytaj o tym wierszu poniżej
            }
        } else if (window.ActiveXObject) { // IE
            try {
                http_request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    http_request = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {
                }
            }
        }

        if (!http_request) {
            alert('Poddaję się :( Nie mogę stworzyć instancji obiektu XMLHTTP');
            return false;
        }
        http_request.onreadystatechange = function () {
            alertContents(http_request, o);
        };
        http_request.open('POST', url, true);
        http_request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

        ajaxParams = prepareParams(params);
        http_request.send(ajaxParams);
    }

function alertContents(http_request, o) {
    var opts;
    if (http_request.readyState == 4) {
        opts = {
            statusCode: http_request.status,
            responseText : http_request.responseText
        };
        o.ajaxCallback(opts);
    }

}

function prepareParams(params) {
    var ajaxParams = "";

    if(params){
        for (var k in params) {
            if (params.hasOwnProperty(k)) {
                ajaxParams += k + "=" + params[k] + "&";
            }
        }
        return ajaxParams.substring(0, ajaxParams.length - 1);
    }
    else{
        return null;
    }
}



