/**
 * Created by DELL on 2015-09-09.
 */
window.addEventListener('resize', function(){
    manageHover(window.innerWidth);
    checkWidth();
});

window.addEventListener('load', function() {
    manageHover(window.innerWidth);
    pagination.init();
});

var pagination = {
    container: null,
    pages: null,

    getActivePage: function(pages){
        var i,
            pageNumber = 0;

        for(i=0; i < pages.length; i++){
            if(pages[i].className == 'active-page'){
                pageNumber = pages[i].id;
            }
        }
        return pageNumber;
    },

    setEventButton: function(){
        var th = this,
            o = pagination,
            currPage,
            arrowId;

            arrowId = this.id;
            currPage = parseInt(o.getActivePage(o.pages), 10);

            if(arrowId == 'prevPage'){
                currPage = currPage-1;
            }
            else if(arrowId == 'nextPage'){
                currPage = currPage+1;
            }
            window.places.init(currPage);
    },

    init: function(){
        var th = this,
            arrowThis,
            arrows,
            pages,
            container,
            i;

        container = document.getElementById("pagination");
        arrows = container.getElementsByTagName("button");
        pages = container.getElementsByTagName("a");
        th.pages = pages;
        th.container = container;


        for(i=0; i < arrows.length; i++){
            arrowThis = arrows[i];
            arrowThis.onclick = th.setEventButton;
        }
    }

};