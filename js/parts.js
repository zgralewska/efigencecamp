/**
 * Created by DELL on 2015-09-06.
 */
var menu = {
    page: "",
    url : "../html/parts/menu.html",
    element: "navMenu",
    ajaxCallback : function(opts){
        var subpageActive,
            mainActive;

        if(opts.statusCode == 200){
            document.getElementById(this.element).innerHTML = opts.responseText;

            if(this.page == "subpage"){
                subpageActive = document.getElementById('menuPlaces');
                subpageActive.className = 'active-item';

            }
            else if(this.page == "main" ){
                mainActive = document.getElementById('menuWhatIsThat');
                mainActive.className = 'active-item';
                mainActive.href = '#description';

                menuItemsClick.init();
            }
            checkWidth();
        }
    },
    getMenu : function(page){
        this.page = page;
        makeRequestGET(this.url, null, this);
    }
};
var topnav = {
    url : "../html/parts/topnav.html",
    element: "optTop",
    ajaxCallback : function(opts){
        if(opts.statusCode == 200){
            document.getElementById(this.element).innerHTML = opts.responseText;
        }
    },
    getTopNav : function(){
        makeRequestGET(this.url, null, this);
    }
};
var footer = {
    url : "../html/parts/footer.html",
    element: "footer",
    ajaxCallback : function(opts){
        if(opts.statusCode == 200){
            document.getElementById(this.element).innerHTML = opts.responseText;
        }
    },
    getFooter : function(){
        makeRequestGET(this.url, null, this);
    }
};

var menuItemsClick = {
    items: [],
    setEvent: function(e){
        e.preventDefault();

        var th = this,
            context = menuItemsClick,
            id,
            element,
            i;

        id= th.id;

        for(i=0; i < context.items.length; i++) {
            context.items[i].className = "";
        }
        this.className = 'active-item';

        switch(id){
            case 'menuWhatIsThat':
                element = document.getElementById('description');
                break;
            case 'menuNews':
                element = document.getElementById('news');
                break;
            case 'menuContact':
                element = document.getElementById('contact');
                break;
        }
        element.scrollIntoView();
    },
    init: function(){
        var
            th = this,
            menu,
            menuItems,
            i;

        menu = document.getElementById('navMenu');
        menuItems = menu.getElementsByTagName('a');

        for(i=0; i < menuItems.length; i++) {
            if(menuItems[i].id != 'menuPlaces'){
                th.items.push(menuItems[i]);
                menuItems[i].onclick = th.setEvent;
            }
        }

    }
};


window.addEventListener('load', function(){
    var page = document.getElementsByTagName('body')[0].getAttribute("data-page");

    menu.getMenu(page);
    topnav.getTopNav();
    footer.getFooter();
});

function checkWidth() {
    var menuItems = document.getElementById("menuItems"),
        searchInput = document.getElementById("navMenu3"),
        logo = document.getElementById("navMenu1");

    if (window.innerWidth < 768) {
        insertAfter(searchInput, logo);
    }
    else if(logo.nextSibling.id == "navMenu3"){
        insertAfter(searchInput, menuItems);
    }
}
